(function() {
  'use strict';

  angular
    .module('govehack2')
    .controller('WaterLevelController', WaterLevelController);

  /** @ngInject */
  function WaterLevelController($interval, $http, $mdDialog, $mdToast) {
    var vm = this;

    var simluationData = [
      {
        time: moment(),
        waterLevel: 11.7
      },
      {
        time: moment(),
        waterLevel: 11.7
      },
      {
        time: moment(),
        waterLevel: 11.7
      },
      {
        time: moment(),
        waterLevel: 11.8
      },
      {
        time: moment(),
        waterLevel: 11.8
      },
      {
        time: moment(),
        waterLevel: 11.9
      },
      {
        time: moment(),
        waterLevel: 12
      },
      {
        time: moment(),
        waterLevel: 12.1
      },
      {
        time: moment(),
        waterLevel: 12.4
      },
      {
        time: moment(),
        waterLevel: 12.6
      },
      {
        time: moment(),
        waterLevel: 12.8
      },
      {
        time: moment(),
        waterLevel: 12.9
      },
      {
        time: moment(),
        waterLevel: 13
      },
      {
        time: moment(),
        waterLevel: 13.3
      },
      {
        time: moment(),
        waterLevel: 13.5
      },
      {
        time: moment(),
        waterLevel: 13.5
      }
    ];

    var dataIndex = 0;
    var dataCap = simluationData.length - 1;


    // shim layer with setTimeout fallback
    window.requestAnimFrame = (function(){
      return  window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        function( callback ){
          window.setTimeout(callback, 1000 / 60);
        };
    })();


    window.onload=function(){
      $mdDialog.show(
        $mdDialog.alert()
          .parent(angular.element(document.querySelector('#popupContainer')))
          .clickOutsideToClose(true)
          .title('Claimer')
          .textContent('This is a demonstration of showing real-time water level. ' +
          'If you input your phone number and subscribe the alert, ' +
          'you will receive a simulated alert message from the creator. ')
          .ariaLabel('Alert Dialog Demo')
          .ok('Got it!')
      );


      /**
       *    Wave oscillators by Ken Fyrstenberg Nilsen
       *    http://abdiassoftware.com/
       *
       *    CC-Attribute 3.0 License
       */
      var ctx = canvas.getContext('2d'),
        w, h;

      var canvasSize = 250;
      var waterHeightRatio = 0.3; // should be between 0.1 to 0.9, 0.1 mean high, 0.9 mean low

      //var canvasSize = window.innerWidth;     // canvas.width = w = window.innerWidth * 1.1;;

      canvas.width = w = canvasSize * 1;
      canvas.height = h = canvasSize * 1;

      //points = new Array(count);
      var osc1 = new osc(),
        osc2 = new osc(),
        osc3 = new osc(),
        horizon = h * waterHeightRatio,
        count = 40,
        step = Math.ceil(w / count),
        buffer = new ArrayBuffer(count * 4),
        points = new Float32Array(buffer);

      osc1.max = 15;//h * 0.7;

      osc2.max = 5;
      osc2.speed = 0.03;


      function fill() {
        for(var i = 0; i < count; i++) {
          points[i] = mixer(osc1, osc2, osc3);
        }
      }
      fill();

      var normal = '#4FC3F7';
      var medium = '#FFEB3B';
      var warning = '#FF5722';
      var danger = '#e53935';

      ctx.lineWidth = 20;
      ctx.strokeStyle = warning;
      ctx.fillStyle = warning;

      function loop() {

        var i;

        /// move points to the left
        for(i = 0; i < count - 1; i++) {
          points[i] = points[i + 1];
        }

        /// get a new point
        points[count - 1] = mixer(osc1, osc2, osc3);

        ctx.clearRect(0, 0, w, h);
        //ctx.fillRect(0, 0, w, h);

        /// render wave
        ctx.beginPath();
        ctx.moveTo(-5, points[0]);

        for(i = 1; i < count; i++) {
          ctx.lineTo(i * step, points[i]);
        }

        ctx.lineTo(w, h);
        ctx.lineTo(-5, h);
        ctx.lineTo(-5, points[1]);

        ctx.stroke();
        ctx.fill();
      }

      /// oscillator object
      function osc() {

        this.variation = 0.4;
        this.max = 8;
        this.speed = 0.02;

        var me = this,
          a = 0,
          max = getMax();

        this.getAmp = function() {

          a += this.speed;

          if (a >= 2.0) {
            a = 0;
            max = getMax();
          }

          return max * Math.sin(a * Math.PI);
        }

        function getMax() {
          return Math.random() * me.max * me.variation +
            me.max * (1 - me.variation);
        }

        return this;
      }

      function mixer() {

        var d = arguments.length,
          i = d,
          sum = 0;

        if (d < 1) return 0;

        while(i--) sum += arguments[i].getAmp();

        return sum / d + horizon;
      }

      (function animloop(){
        requestAnimFrame(animloop);
        loop();
      })();

      vm.changeColour = function () {
        ctx.strokeStyle = normal;
        ctx.fillStyle = normal;
      };

      vm.simluate = function () {
        dataIndex = 0;

        var stopSimulation = $interval(function() {
          var currentWaterLevel = loadData();

          changeWaterLevelHigher(currentWaterLevel);

          changeWaterLevelColour();

          checkIfAlertRequired();

          if(dataIndex === dataCap ) {
            //reset simulation
            dataIndex = 0;
            $interval.cancel(stopSimulation);
          }
        }, 1000);
      };

      function loadData() {
        return simluationData[dataIndex++].waterLevel;
      }

      var maxWaterLevel = 13.5;
      var waterLevelRange = 13.5 - 11.7;

      function changeWaterLevelHigher(waterLevel) {
        var changePercentage = (maxWaterLevel - waterLevel) / waterLevelRange * 0.8;
        waterHeightRatio = changePercentage + 0.1;
        horizon = h * waterHeightRatio;
      }

      function checkIfAlertRequired() {
        if(waterHeightRatio < 0.3) {
          sendSmsAlert();
        }
      }

      function sendSmsAlert () {
        var baseUrl = 'http://192.168.1.192:3933';
        $http
          .post(baseUrl + '/api/sms', {PhoneNumber: vm.userPhoneNumber})
          .success(function (data, status, headers, config) {
            successToast();
          })
          .error(function (data, status, headers, config) {
            errorToast();
          });

        successToast();
      }

      function successToast() {

        var toast = $mdToast.simple()
          .textContent('An alert message has been sent!')
          .position('top right')
          .action('OK')
          .hideDelay(5000);

        $mdToast.show(toast);

      }

      function errorToast() {

        var toast = $mdToast.simple()
          .textContent('Something went wrong!')
          .position('top right')
          .action('OK')
          .hideDelay(5000);

        $mdToast.show(toast);
      }

      function changeWaterLevelColour() {
        if(waterHeightRatio < 0.3) {
          ctx.strokeStyle = danger;
          ctx.fillStyle = danger;
        }
        else if (waterHeightRatio < 0.5) {
          ctx.strokeStyle = warning;
          ctx.fillStyle = warning;
        }
        else if (waterHeightRatio < 0.7) {
          ctx.strokeStyle = medium;
          ctx.fillStyle = medium;
        }
        else {
          ctx.strokeStyle = normal;
          ctx.fillStyle = normal;
        }
      }
    }

  }
})();
