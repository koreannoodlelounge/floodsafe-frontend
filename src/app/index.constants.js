/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('govehack2')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
