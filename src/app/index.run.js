(function() {
  'use strict';

  angular
    .module('govehack2')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
