(function() {
  'use strict';

  angular
    .module('govehack2')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      //.state('home', {
      //  url: '/',
      //  templateUrl: 'app/main/main.html',
      //  controller: 'MainController',
      //  controllerAs: 'main'
      //})
      .state('home', {
        url: '/',
        templateUrl: 'app/waterLevel/waterLevel.html',
        controller: 'WaterLevelController',
        controllerAs: 'waterLevel'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
